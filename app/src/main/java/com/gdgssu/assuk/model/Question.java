package com.gdgssu.assuk.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Question {
    private static final String TAG = "Question";

    public int id;
    public String title;
    public String content;
    public String profile;
    public String image;
    public String regist;
    public User user;
    public JSONArray like;
    public JSONArray comment;

    public Question convert(JSONObject object){
        final Question question = new Question();
        try {
            question.id = object.getInt("id");
            question.title = object.getString("questionTitle");
            question.content = object.getString("questionContent");
            question.like = object.getJSONArray("rel_question_like");
            question.comment = object.getJSONArray("rel_question_comment");
            question.regist = object.getString("regist");
            question.user = new User().convert(object.getJSONObject("user"));

            if(!object.getString("questionImages").equals("[]")) {
                question.image = new JSONArray(object.getString("questionImages")).getJSONObject(0).getString("url");
            }else{
                question.image = "";
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return question;
    }
}