package com.gdgssu.assuk.model;

import org.json.JSONException;
import org.json.JSONObject;

public class User {
    public int id;
    public String email;
    public String nick;
    public String profile;

    public User convert(JSONObject object){
        User user = new User();
        try{
            user.id = object.getInt("id");
            user.email = object.getString("userEmail");
            user.nick = object.getString("userNick");
            user.profile = object.getString("userProfile");
        }catch (JSONException e){
            e.printStackTrace();
        }
        return user;
    }
}
