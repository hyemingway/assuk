package com.gdgssu.assuk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;


public class AssukBaseData extends Application {
    public static String apiURL = "http://growingdever.cafe24.com:4000";
    private static Context context;

    // USER DATA
    public static int userID = 0;
    public static String userNick = "";
    public static String userProfileUrl = "";

    public static SharedPreferences pref = null;

    public void onCreate(){
        super.onCreate();

        Log.d("AAA", "test");

        AssukBaseData.context = getApplicationContext();
        pref = getSharedPreferences("UserInfo", Context.MODE_PRIVATE);

    }

    public static Context getAppContext() {
        return AssukBaseData.context;
    }

    public static RequestParams flaskRestlessQuery(String query){
        RequestParams param = new RequestParams();
        try {
            JSONObject object = null;
            object = new JSONObject(query);

            param.add("q", object.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return param;
    }
}
