package com.gdgssu.assuk.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.gdgssu.assuk.Activity.TimelineActivity;
import com.gdgssu.assuk.AssukBaseData;
import com.gdgssu.assuk.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class searchDialog extends Dialog implements View.OnClickListener {
    private static final String TAG = "searchDialog";
    private EditText edit_search;
    private Button btn_search;
    private Activity activity;

    public searchDialog(Activity activity) {
        super(activity);
        this.activity = activity;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.search_dialog);

        setLayout();
    }

    public void setLayout(){
        edit_search = (EditText)findViewById(R.id.editText1);
        btn_search = (Button)findViewById(R.id.button1);

        edit_search.setText("");
        btn_search.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if(!edit_search.getText().toString().equals("")){
            search(edit_search.getText().toString());
        }else {
            Toast.makeText(getContext(), "검색어를 입력해주세요.", Toast.LENGTH_SHORT).show();
        }
    }

    private void search(String keyword) {
        Bundle data = new Bundle();
        data.putString("keyword",keyword);
        Message msg = new Message();
        msg.setData(data);
        Handler handler = new Handler();
        handler.sendMessage(msg);
//        Toast.makeText(getContext(), "search " + keyword, Toast.LENGTH_SHORT).show();
        this.cancel();
    }
}
