package com.gdgssu.assuk.dialog;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.gdgssu.assuk.R;

/**
 * Created by DELL on 2014-11-25.
 */
public class alertDialog extends Dialog implements View.OnClickListener {
    private static final String TAG = "ALERT_DIALOG";
    private Button pos;
    private Button neg;

    public alertDialog(Context context) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_alert);
        setLayout();
    }

    private void setLayout() {
        pos = (Button) findViewById(R.id.pos_button);
        neg = (Button) findViewById(R.id.neg_button);

        pos.setOnClickListener(this);
        neg.setOnClickListener(this);

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.pos_button: //질문 작성 취소
                this.cancel();
                Log.d(TAG, "posintive and canceled");
                break;
            case R.id.neg_button: //질문 작성 계속하기
                this.dismiss();
                Log.d(TAG, "negative and dismissed");
                break;
        }
    }
}
