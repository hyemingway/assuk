package com.gdgssu.assuk.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.gdgssu.assuk.Activity.TimelineActivity;
import com.gdgssu.assuk.AssukBaseData;
import com.gdgssu.assuk.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;


public class LoginDialogFragment extends Dialog implements View.OnClickListener {
    public static final String TAG = "LoginDialogFragment";

    private EditText edit_email;
    private EditText edit_pwd;
    private Button btn_login;

    private Activity activity;

    private boolean autoLogin;
    private CheckBox check_auto;

    public LoginDialogFragment(Activity activity) {
        super(activity);
        this.activity = activity;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fragment_login);

        setLayout();
    }

    public void setLayout() {
        edit_email = (EditText) findViewById(R.id.edit_email);
        edit_pwd = (EditText) findViewById(R.id.edit_pwd);
        btn_login = (Button) findViewById(R.id.btn_login_submit);

        check_auto = (CheckBox) findViewById(R.id.checkBox_login);

//        //TODO : remove default setting
//        edit_email.setText("admin@gmail.com");
//        edit_pwd.setText("admin");

        btn_login.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_login_submit:
                if (autologin()) {
                    Log.d(TAG, "autoLogin func return check : " + autologin());
                    String userID = edit_email.getText().toString();
                    String userPW = edit_pwd.getText().toString();
                    if (userID != null && userPW != null) {
                        saveUser(userID, userPW);
                    }
                }
                login(edit_email.getText().toString(), edit_pwd.getText().toString());
                break;
        }
    }

    private boolean validateCheck(int email_length, int pwd_length) {
        if ((email_length > 4) && (pwd_length > 4)) {
            return true;
        }
        return false;
    }

    private void saveUser(String userID, String userPW) {
        SharedPreferences.Editor editor = AssukBaseData.pref.edit();
        editor.putString("userID", userID);
        editor.putString("userPW", userPW);
        editor.commit();
        Log.d(TAG, "pref check : " + AssukBaseData.pref.getString("userID", ""));
    }

    private void login(String email, String pwd) {
        JSONObject param = new JSONObject();
        AsyncHttpClient client = new AsyncHttpClient();
        StringEntity entity;

        try {
            param.put("userEmail", email);
            param.put("userPW", pwd);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            entity = new StringEntity(param.toString());

            client.post(getContext(), AssukBaseData.apiURL + "/api/v1.0/auth", entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d(TAG, statusCode + "");
                    Log.d(TAG, response.toString());

                    if (statusCode == 200) {
                        try {
                            if (response.getBoolean("login") == true) {

                                AssukBaseData.userID = response.getInt("id");
                                AssukBaseData.userNick = response.getString("nick");
                                AssukBaseData.userProfileUrl = response.getString("profile");

                                Intent intent = new Intent(getContext(), TimelineActivity.class);
                                activity.startActivity(intent);
                                activity.finish();
                            } else {
                                Toast.makeText(getContext(), "아이디 혹은 비밀번호가 존재하지 않습니다.", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getContext(), "서버연결에 문제가 있습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private boolean autologin() {
        autoLogin = check_auto.isChecked();
        Log.d(TAG, "autologin return value check : " + autoLogin);
        return autoLogin;
    }
}
