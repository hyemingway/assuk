package com.gdgssu.assuk.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gdgssu.assuk.Activity.TimelineActivity;
import com.gdgssu.assuk.AssukBaseData;
import com.gdgssu.assuk.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class RegistDialogFragment extends Dialog implements View.OnClickListener {
    public static final String TAG = "RegistDialogFragment";

    EditText edit_email;
    EditText edit_pwd;
    EditText edit_nick;
    Button btn_regist;

    public RegistDialogFragment(Context context) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fragment_regist);

        setLayout();
    }

    public void setLayout(){
        edit_email = (EditText)findViewById(R.id.edit_email);
        edit_pwd = (EditText)findViewById(R.id.edit_pwd);
        edit_nick = (EditText)findViewById(R.id.edit_nick);
        btn_regist = (Button)findViewById(R.id.btn_regist_submit);

        btn_regist.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_regist_submit:
                int email_length = edit_email.getText().toString().length();
                int pwd_length = edit_pwd.getText().toString().length();
                if(validateCheck(email_length, pwd_length)){
                    regist(edit_email.getText().toString(),
                            edit_pwd.getText().toString(),
                            edit_nick.getText().toString());
                }else{
                    Toast.makeText(getContext(), "4글자 이상 입력해주세요. ",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean validateCheck(int email_length, int pwd_length) {
        if((email_length > 4) && (pwd_length > 4) ){
            return true;
        }
        return false;
    }

    public void regist(String email, String pwd, String nick){
        JSONObject param = new JSONObject();
        AsyncHttpClient client = new AsyncHttpClient();
        StringEntity entity;

        try{
            param.put("userEmail", email);
            param.put("userPW", pwd);
            param.put("userNick", nick);
        }catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            entity = new StringEntity(param.toString(), HTTP.UTF_8);

            client.post(getContext(), AssukBaseData.apiURL + "/api/v1.0/regist", entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d(TAG, statusCode + "");
                    Log.d(TAG, response.toString());

                    if (statusCode == 200) {
                        try{
                            if(response.getBoolean("regist") == true){
                                cancelDialog();
                                Toast.makeText(getContext(), "회원가입에 성공했습니다!", Toast.LENGTH_SHORT).show();
                            }else{
                                Toast.makeText(getContext(), response.getString("msg"), Toast.LENGTH_SHORT).show();
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public void cancelDialog(){
        this.cancel();
    }
}
