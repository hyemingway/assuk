package com.gdgssu.assuk.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;

import com.gdgssu.assuk.AssukBaseData;
import com.gdgssu.assuk.R;
import com.gdgssu.assuk.adapter.CardListAdapter;
import com.gdgssu.assuk.dialog.searchDialog;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class TimelineActivity extends Activity
        implements SwipeRefreshLayout.OnRefreshListener, ListView.OnItemClickListener, View.OnClickListener {

    private static final String TAG = "TimelineActivity";

    private SwipeRefreshLayout swipe_timeline;
    private ListView list_timeline;
    private CardListAdapter cardListAdapter;
    private ImageView btn_write;

    private ListView list_drawer;
    private DrawerLayout drawerlayout;
    private ActionBarDrawerToggle drawerToggle;
    private ArrayList<String> arrayList_menu = new ArrayList<String>();

    private Spinner spinner_tag;
    private ArrayAdapter<String> adapter_tag;
    private ArrayList<String> arrlist_tag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);

        setTagData();
        setLayout();
    }

    @Override
    public void onRefresh() {
        cardListAdapter.initialize_swipe(this, swipe_timeline);
    }

    private void setLayout() {

        spinner_tag = (Spinner)findViewById(R.id.spinner_tag);
        adapter_tag = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, arrlist_tag);
        spinner_tag.setAdapter(adapter_tag);
        spinner_tag.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                RequestParams param;

                if(i == 0){
                    param = AssukBaseData.flaskRestlessQuery("{\"order_by\":[{\"field\": \"regist\", \"direction\": \"desc\"}]}");
                }else {
                    param = AssukBaseData.flaskRestlessQuery(
                            "{\"filters\":[{\"name\":\"questionTags\", \"op\":\"==\", \"val\":\"" + arrlist_tag.get(i) + "\"}]}");
                }
                cardListAdapter.initialize(getApplicationContext(), "/api/v1.0/question", param);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        arrayList_menu.add("  타임라인");
        arrayList_menu.add("  나의질문");
        arrayList_menu.add("  검색");
        arrayList_menu.add("  로그아웃");

        swipe_timeline = (SwipeRefreshLayout) findViewById(R.id.swipe_timeline);
        swipe_timeline.setOnRefreshListener(this);
        swipe_timeline.setColorSchemeColors(R.color.color1, R.color.color2, R.color.color3, R.color.color4);

        cardListAdapter = new CardListAdapter(this);
        RequestParams param = AssukBaseData.flaskRestlessQuery("{\"order_by\":[{\"field\": \"regist\", \"direction\": \"desc\"}]}");
        cardListAdapter.initialize(this, "/api/v1.0/question", param);

        btn_write = (ImageView)findViewById(R.id.btn_write);
        btn_write.setOnClickListener(this);

        list_timeline = (ListView) findViewById(R.id.list_timeline);
        list_timeline.setDivider(null);
        list_timeline.setAdapter(cardListAdapter);
        list_timeline.setOnItemClickListener(this);

        list_drawer = (ListView) findViewById(R.id.left_drawer);
        list_drawer.setDivider(null);
        list_drawer.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, arrayList_menu));
        ColorDrawable grey = new ColorDrawable(this.getResources().getColor(R.color.grey));
        list_drawer.setDivider(grey);
        list_drawer.setDividerHeight(1);
        list_drawer.setOnItemClickListener(this);

        drawerlayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(this, drawerlayout,
                R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActionBar().setTitle(R.string.drawer_close);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActionBar().setTitle(R.string.drawer_open);
            }
        };
        drawerlayout.setDrawerListener(drawerToggle);

        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        if(view.getId() == -1){

            int id = (int) adapterView.getAdapter().getItemId(i);
            Intent intent = new Intent(this, QuestionActivity.class);
            intent.putExtra("url", AssukBaseData.apiURL + "/api/v1.0/question/" + id);
            startActivity(intent);

        }else {
            RequestParams param;
            switch (i) {
                case 0: // 타임라인
                    param = AssukBaseData.flaskRestlessQuery("{\"order_by\":[{\"field\": \"regist\", \"direction\": \"desc\"}]}");
                    cardListAdapter.initialize(this, "/api/v1.0/question", param);
                    break;
                case 1: // 나의질문
                    param = AssukBaseData.flaskRestlessQuery(
                            "{\"filters\":[{\"name\":\"user_id\", \"op\":\"==\", \"val\":" + AssukBaseData.userID + "}]}");
                        cardListAdapter.initialize(this, "/api/v1.0/question", param);
                    break;
                case 2: // 검색
                    openSearch();
                    break;
                case 3: // 로그아웃
                    SharedPreferences.Editor editor = AssukBaseData.pref.edit();
                    Log.d("pref check", AssukBaseData.pref.getString("userID","defalut"));

                    editor.remove("userID");
                    editor.remove("userPW");
                    editor.commit();
                    Log.d("pref check", AssukBaseData.pref.getString("userID","defalut"));

                    Intent intent_logout = new Intent(TimelineActivity.this, LoginActivity.class);
                    startActivity(intent_logout);
                    finish();
            }
            drawerlayout.closeDrawer(list_drawer);
        }
    }

    protected void onPostCreate(Bundle savedInstanceState){
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.timeline, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_write:
                Intent intent = new Intent(this, WriteFeedActivity.class);
                startActivity(intent);
                break;
        }
    }

    private void setTagData(){
        arrlist_tag = new ArrayList<String>();
        arrlist_tag.add("전체보기");

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(getApplicationContext(), AssukBaseData.apiURL + "/api/v1.0/tag", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray objectsArray = response.getJSONArray("objects");
                    for (int i = 0; i < objectsArray.length(); i++) {
                        JSONObject object = objectsArray.getJSONObject(i);
                        arrlist_tag.add(object.getString("tagName"));
                    }
                    adapter_tag.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

//    private void openWrite() {
//        Intent goWrite = new Intent(TimelineActivity.this, WriteFeedActivity.class);
//        startActivity(goWrite);
//    }

    private void openSearch() {
        new searchDialog(this).show();
        Handler commanderHdr = new Handler(){
            public void handleMessage(Message msg){
                String keyword = msg.getData().getString("data");
                search(keyword);
            }
        };
    }

    private void search(String keyword) {
        JSONObject param = new JSONObject();
        AsyncHttpClient client = new AsyncHttpClient();
        StringEntity entity;

        try {
            param.put("searchCategory", 1);
            param.put("searchTag",keyword);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            entity = new StringEntity(param.toString());

            client.post(getApplicationContext(), AssukBaseData.apiURL + "/api/v1.0/search", entity, "application/json", new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    String res = new String(responseBody);
                    try {
                        JSONArray responseJSONArray = new JSONArray(res);
                        Log.d(TAG + " responseJSONArray", responseJSONArray.toString());
                        Log.d(TAG, "responseJSONArray length " + responseJSONArray.length());
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e(TAG, e.toString());
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.d(TAG, "status :" + statusCode);
                }
            });

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }
}
