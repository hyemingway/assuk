package com.gdgssu.assuk.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.gdgssu.assuk.R;
import com.gdgssu.assuk.fragment.LoginDialogFragment;
import com.gdgssu.assuk.fragment.RegistDialogFragment;

public class LoginActivity extends Activity implements View.OnClickListener{
    LoginDialogFragment loginDialogFragment;
    RegistDialogFragment registDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);

        loginDialogFragment = new LoginDialogFragment(this);
        registDialogFragment = new RegistDialogFragment(this);

        Button btn_login = (Button)findViewById(R.id.btn_login);
        Button btn_regist = (Button)findViewById(R.id.btn_regist);

        btn_login.setOnClickListener(this);
        btn_regist.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_login:
                loginDialogFragment.show();
                break;
            case R.id.btn_regist:
                registDialogFragment.show();
                break;
        }
    }
}
