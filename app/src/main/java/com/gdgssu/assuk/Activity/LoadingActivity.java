package com.gdgssu.assuk.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.gdgssu.assuk.AssukBaseData;
import com.gdgssu.assuk.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;


public class LoadingActivity extends Activity {
    private static final String TAG = "LoadingActivity";
    //Splash Activity
    private static int SPLASH_TIME_OUT = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if(AssukBaseData.pref.getString("userID","") != "" && AssukBaseData.pref.getString("userPW","")!= ""){
                    autoLoginOn();
                }else{
                    Intent toMain = new Intent(LoadingActivity.this, LoginActivity.class);
                    startActivity(toMain);
                }

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private void autoLoginOn(){
        login(AssukBaseData.pref.getString("userID",""), AssukBaseData.pref.getString("userPW",""));
        Log.d(TAG, AssukBaseData.pref.getString("UserPW","") + "***");

    }

    private void login(String email, String pwd) {
        JSONObject param = new JSONObject();
        AsyncHttpClient client = new AsyncHttpClient();
        StringEntity entity;

        try {
            param.put("userEmail", email);
            param.put("userPW", pwd);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            entity = new StringEntity(param.toString());

            client.post(getApplicationContext(), AssukBaseData.apiURL + "/api/v1.0/auth", entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d(TAG, statusCode + "");
                    Log.d(TAG, response.toString());

                    if (statusCode == 200) {
                        try {
                            if (response.getBoolean("login") == true) {

                                AssukBaseData.userID = response.getInt("id");
                                AssukBaseData.userNick = response.getString("nick");
                                AssukBaseData.userProfileUrl = response.getString("profile");

                                Intent intent = new Intent(getApplicationContext(), TimelineActivity.class);
                                startActivity(intent);
                                finish();
                            } else {
//                                Toast.makeText(getApplicationContext(), "아이디 혹은 비밀번호가 존재하지 않습니다.", Toast.LENGTH_SHORT).show();
                                Intent toMain = new Intent(LoadingActivity.this, LoginActivity.class);
                                startActivity(toMain);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "서버연결에 문제가 있습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }
}
