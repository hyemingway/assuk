package com.gdgssu.assuk.Activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.gdgssu.assuk.AssukBaseData;
import com.gdgssu.assuk.R;
import com.gdgssu.assuk.photoViewLibrary.PhotoView;
import com.gdgssu.assuk.photoViewLibrary.PhotoViewAttacher;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

public class ImageActivity extends Activity {

    private static final String TAG = "ImageActivity";
    private String url;
    private ImageView contentImg;
    private PhotoViewAttacher photoviewAttacher;
    private Drawable bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        getImageSrc();

        url = AssukBaseData.apiURL + url;
        //"http://ssu.gdg.kr:4000/assets/upload/2014_12_02_21_40_06_685609.png";
        contentImg = (ImageView) findViewById(R.id.imageView);

        if (android.os.Build.VERSION.SDK_INT > 9) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

            StrictMode.setThreadPolicy(policy);

        }

        bitmap = createDrawableFromUrl(url);
        contentImg.setImageDrawable(bitmap);
        photoviewAttacher = new PhotoViewAttacher(contentImg);
    }

    public void getImageSrc() {
        Intent i = getIntent();
        url = i.getStringExtra("url");
        Log.d(TAG, url + "");

    }

    public static Drawable createDrawableFromUrl(String imageWebAddress) {
        Drawable drawable = null;

        try {
            InputStream inputStream = new URL(imageWebAddress).openStream();
            drawable = Drawable.createFromStream(inputStream, null);
            inputStream.close();
        } catch (MalformedURLException ex) {
        } catch (IOException ex) {
        }

        return drawable;
    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.image, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
*/
}
