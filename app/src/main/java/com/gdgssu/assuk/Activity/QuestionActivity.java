package com.gdgssu.assuk.Activity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.gdgssu.assuk.AssukBaseData;
import com.gdgssu.assuk.R;
import com.gdgssu.assuk.adapter.CommentListAdapter;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;


public class QuestionActivity extends Activity {

    private static final String TAG = "QuestionActivity";

    private String source_url;
    private JSONObject questionData;

    private LinearLayout linear_comment;
    private EditText edit_comment;
    private ListView list_comment;
    private CommentListAdapter commentListAdapter;

    private ImageView imgView;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question);

        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);

        Intent intent = getIntent();
        source_url = intent.getStringExtra("url");
        setQuestion(source_url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.question, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setQuestion(String url) {
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(getApplicationContext(), url, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(TAG, statusCode + "");
                Log.d(TAG, response.toString());

                if (statusCode == 200) {
                    Log.d(TAG, response.toString());

                    questionData = response;
                    setLayout();
                }
            }
        });
    }

    private void setLayout() {
        try {
            url = new JSONArray(questionData.getString("questionImages")).getJSONObject(0).getString("url");
            Log.d(TAG + "image ****  ", url);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        imgView = (ImageView) findViewById(R.id.img_content);
                //= (ImageView) findViewById(R.id.imageView);
        linear_comment = (LinearLayout) findViewById(R.id.linear_comment);
        list_comment = (ListView) findViewById(R.id.list_comment);
        edit_comment = (EditText) findViewById(R.id.edit_comment);

        commentListAdapter = new CommentListAdapter(questionData);
        list_comment.setAdapter(commentListAdapter);

        linear_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!(edit_comment.getText().toString().equals(""))) {
                    setComment(edit_comment.getText().toString());
                }
                edit_comment.setText("");
                edit_comment.clearFocus();
            }
        });

        list_comment.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(i == 0){
                    if(url != null){
                        Log.d(TAG + "image **** ", url);
                        Intent goDetailImage = new Intent(QuestionActivity.this, ImageActivity.class);
                        goDetailImage.putExtra("url", url);
                        startActivity(goDetailImage);
                    }

                }

            }
        });

    }

    private void setComment(String data) {
        JSONObject param = new JSONObject();
        try {
            param.put("commentContent", data);
            param.put("question_id", questionData.getInt("id"));
            param.put("user_id", AssukBaseData.userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity entity = null;
        try {
            entity = new StringEntity(param.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/json");
        client.post(getApplicationContext(), AssukBaseData.apiURL + "/api/v1.0/comment", entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(TAG, statusCode + "");
                Log.d(TAG, response.toString());
                setQuestion(source_url);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                Log.d(TAG, response.toString());
                Toast.makeText(getApplicationContext(), "댓글쓰기에 실패했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
