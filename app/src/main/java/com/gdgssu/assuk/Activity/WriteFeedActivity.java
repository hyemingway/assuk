package com.gdgssu.assuk.Activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.gdgssu.assuk.AssukBaseData;
import com.gdgssu.assuk.R;
import com.gdgssu.assuk.dialog.alertDialog;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;


public class WriteFeedActivity extends Activity implements View.OnClickListener {

    private static final int GET_PICTURE_URI = 1;
    private static final String TAG = "WriteFeedActivity";

    private Button sendBtn;
    private EditText writeQuest, writeTitle;

    private Spinner categorySpinner;
    private ArrayAdapter<String> adapterCategory;
    private List<String> categoryArr;

    private Spinner tagSpinner;
    private ArrayAdapter<String> adapterTag;
    private List<String> tagArr;

    private AsyncHttpClient client;
    private StringEntity entity;
    private Uri imgUri;
    private boolean imgCheck = false;

    private int mCategoryID = 1;

    private alertDialog alert_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write_feed);

        client = new AsyncHttpClient();

        setCategoryData();
        setTagData();
        setLayout();
    }

    private void setLayout() {
        ActionBar actionBar = getActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(false);

        writeQuest = (EditText) findViewById(R.id.questionText);
        writeTitle = (EditText) findViewById(R.id.questionTitle);
        sendBtn = (Button) findViewById(R.id.sendButton);

        categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
        tagSpinner = (Spinner) findViewById(R.id.tagSpinner);

        adapterCategory = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, categoryArr);
        adapterTag = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, tagArr);

        categorySpinner.setAdapter(adapterCategory);
        categorySpinner.setSelection(1);
        tagSpinner.setAdapter(adapterTag);

        alert_dialog = new alertDialog(this);

        sendBtn.setOnClickListener(this);
    }

    private void setCategoryData() {
        categoryArr = new ArrayList<String>();

        client.get(getApplicationContext(), AssukBaseData.apiURL + "/api/v1.0/category", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray data = response.getJSONArray("objects");
                    for (int i = 0; i < data.length(); i++) {
                        JSONObject object = data.getJSONObject(i);
                        categoryArr.add(object.getString("categoryName"));
                    }
                    adapterCategory.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void setTagData() {
        tagArr = new ArrayList<String>();

        client.get(getApplicationContext(), AssukBaseData.apiURL + "/api/v1.0/tag", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    JSONArray objectsArray = response.getJSONArray("objects");
                    for (int i = 0; i < objectsArray.length(); i++) {
                        JSONObject object = objectsArray.getJSONObject(i);
                        tagArr.add(object.getString("tagName"));
                    }

                    adapterTag.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void openGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType(android.provider.MediaStore.Images.Media.CONTENT_TYPE);
        intent.setData(android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GET_PICTURE_URI);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == GET_PICTURE_URI) {
            if (resultCode == Activity.RESULT_OK) {
                imgUri = data.getData();
            }else{
                imgUri = null;
            }
        }
        ImageView image = (ImageView) findViewById(R.id.imageView);
        try {
            if(imgUri != null){
                Bitmap bm = MediaStore.Images.Media.getBitmap(getContentResolver(), imgUri);
                bm = resizeBitmapImageFn(bm, 200);
                image.setImageBitmap(bm);
            }
        } catch (FileNotFoundException e) {
            Log.e("File not found", e.toString());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("File not found", e.toString());
            e.printStackTrace();
        }
    }

    Bitmap resizeBitmapImageFn(
            Bitmap bmpSource, int maxResolution) {
        //bmpSource 원본 Bitmap 객체, maxResolution 제한 해상도
        int iWidth = bmpSource.getWidth();      //비트맵이미지의 넓이
        int iHeight = bmpSource.getHeight();     //비트맵이미지의 높이
        int newWidth = iWidth;
        int newHeight = iHeight;
        float rate = 0.0f;

        //이미지의 가로 세로 비율에 맞게 조절
        if (iWidth > iHeight) {
            if (maxResolution < iWidth) {
                rate = maxResolution / (float) iWidth;
                newHeight = (int) (iHeight * rate);
                newWidth = maxResolution;
            }
        } else {
            if (maxResolution < iHeight) {
                rate = maxResolution / (float) iHeight;
                newWidth = (int) (iWidth * rate);
                newHeight = maxResolution;
            }
        }
        return Bitmap.createScaledBitmap(
                bmpSource, newWidth, newHeight, true);
        //리사이즈된 이미지 Bitmap 객체
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.write_feed, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_gallery) {
            //openGallery();
            imgCheck = true;
            Toast.makeText(getApplicationContext(), "이미지 업로드는 조금만 더 기다려 주세요 :)", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sendButton:
                submit();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Log.d(TAG, "back button clicked");

        if(isWrite()){
            Log.d(TAG, "isWrite is " + isWrite());
            alert_dialog.setCancelable(false);  //back key 눌렀을 경우 Dialog Cancel 여부 결정
            alert_dialog.show();
            alert_dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    finish();
                }
            });
        }else{
            finish();
        }
    }

    private boolean isWrite() {
        String title = writeTitle.getText().toString();
        String content = writeQuest.getText().toString();
        Log.d(TAG , title + content);
        if(title.equals("") && content.equals("")){
            return false;
        }else{
            return true;
        }
    }

    private boolean submit() {
        String title = writeTitle.getText().toString();
        String content = writeQuest.getText().toString();

        int selectedTagAt = tagSpinner.getSelectedItemPosition();
        int selectedCategoryAt = categorySpinner.getSelectedItemPosition();

        String selectedTag = tagSpinner.getItemAtPosition(selectedTagAt).toString();
        String selectedCategory = categorySpinner.getItemAtPosition(selectedCategoryAt).toString();

        if (title == null || content == null) {
            Toast.makeText(getApplicationContext(), "내용을 입력해주세요.", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            Log.d(TAG, "Category ID :" + mCategoryID);
            Log.d(TAG, "questionContent :" + content);
            Log.d(TAG, "questionTags :" + selectedTag);
            Log.d(TAG, "questionTitle :" + title);
            Log.d(TAG, "user_id :" + AssukBaseData.userID);

            JSONObject param = new JSONObject();
            try {
                param.put("category_id", mCategoryID);
                param.put("questionContent", content);
                param.put("questionTags", selectedTag);
                param.put("questionTitle", title);
                param.put("questionImages", "[]");
                param.put("user_id", AssukBaseData.userID);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                entity = new StringEntity(param.toString(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            client.addHeader("Content-Type", "application/json");
            client.post(getApplicationContext(), AssukBaseData.apiURL + "/api/v1.0/question", entity, "application/json", new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d(TAG, statusCode + "");
                    Log.d(TAG, response.toString());
                    if (statusCode != 201) {
                        Toast.makeText(getApplicationContext(), "글쓰기에 실패했습니다. 잠시후 다시 시도해주세요.", Toast.LENGTH_SHORT).show();
                    }
                    finish();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response) {
                    Log.d(TAG, response.toString());
                }
            });

            return true;
        }
    }
}
