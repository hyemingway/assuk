package com.gdgssu.assuk.adapter;

import com.android.volley.toolbox.NetworkImageView;
import com.gdgssu.assuk.AssukBaseData;
import com.gdgssu.assuk.R;
import com.gdgssu.assuk.manager.VolleySingleton;
import com.gdgssu.assuk.model.Question;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.app.ActionBar;
import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CardListAdapter extends BaseAdapter {
    private final static String TAG = "CardListAdapter";

    private ArrayList<Question> questions;
    private Context timelineContext;

    private String api = "";
    private RequestParams param = null;

    public CardListAdapter(Context context){
        questions = new ArrayList<Question>();
        timelineContext = context;
    }

    @Override
    public int getCount() {
        return questions.size();
    }

    @Override
    public Object getItem(int i) {
        return questions.get(i);
    }

    @Override
    public long getItemId(int i) {
        return questions.get(i).id;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Context context = viewGroup.getContext();

        if ( view == null ) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_card, viewGroup, false);
        }

        TextView txt_cardTitle = (TextView)view.findViewById(R.id.txt_cardTitle);
        TextView txt_cardUserNick = (TextView)view.findViewById(R.id.txt_cardUserNick);
        TextView txt_cardContent = (TextView)view.findViewById(R.id.txt_cardContent);
        TextView txt_like = (TextView)view.findViewById(R.id.txt_like);
        TextView txt_comment = (TextView)view.findViewById(R.id.txt_comment);

        NetworkImageView img_profile = (NetworkImageView)view.findViewById(R.id.img_profile);
        NetworkImageView img_preview = (NetworkImageView)view.findViewById(R.id.img_preview);

        txt_cardTitle.setText(questions.get(i).title);
        txt_cardContent.setText(questions.get(i).content);
        txt_cardUserNick.setText(questions.get(i).user.nick);

        txt_like.setText(questions.get(i).like.length() + "");
        txt_comment.setText(questions.get(i).comment.length() + "");

        img_profile.setImageUrl(null, null);
        img_preview.setImageUrl(null, null);
        img_profile.setImageUrl(AssukBaseData.apiURL + questions.get(i).user.profile, VolleySingleton.getInstance().getImageLoader());
        img_preview.setImageUrl(AssukBaseData.apiURL + questions.get(i).image, VolleySingleton.getInstance().getImageLoader());

        return view;
    }

    private void setList(JSONArray objects){
        JSONObject object;
        questions.clear();
        try {
            for(int i = 0; i < objects.length(); i++){
                object = objects.getJSONObject(i);
                questions.add(new Question().convert(object));
            }
            notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void initialize_swipe(final Context context, final SwipeRefreshLayout swipe){
        AsyncHttpClient client = new AsyncHttpClient();
        Log.d(TAG, AssukBaseData.apiURL + api);

        client.get(context, AssukBaseData.apiURL + api, param, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(TAG, "statusCode :" + statusCode);
                Log.d(TAG, response.toString());

                try {
                    if (statusCode == 200) {
                        Log.d(TAG, response.toString());
                        setList(response.getJSONArray("objects"));
                        if(swipe != null){
                            swipe.setRefreshing(false);
                        }
                        if(response.getJSONArray("objects").length() == 0){
                            Toast.makeText(context, "질문이 없습니다.", Toast.LENGTH_SHORT).show();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void initialize(final Context context, String api, RequestParams param) {
        this.api = api;
        this.param = param;

        AsyncHttpClient client = new AsyncHttpClient();
        Log.d(TAG, AssukBaseData.apiURL + api);

        client.get(context, AssukBaseData.apiURL + api, param, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(TAG, "statusCode :" + statusCode);
                Log.d(TAG, response.toString());

                try {
                    if (statusCode == 200) {
                        Log.d(TAG, response.toString());
                        setList(response.getJSONArray("objects"));

                        if(response.getJSONArray("objects").length() == 0){
                            Toast.makeText(context, "질문이 없습니다.", Toast.LENGTH_SHORT).show();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
