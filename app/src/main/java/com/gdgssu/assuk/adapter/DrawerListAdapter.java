package com.gdgssu.assuk.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

/**
 * Created by DELL on 2014-11-20.
 */
public class DrawerListAdapter extends BaseAdapter{
    private ArrayList<String> array_drawer;

    public DrawerListAdapter(){
        array_drawer = new ArrayList<String>();
    }
    @Override
    public int getCount() {
        return array_drawer.size();
    }

    @Override
    public Object getItem(int i) {
        return array_drawer.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return null;
    }
}
