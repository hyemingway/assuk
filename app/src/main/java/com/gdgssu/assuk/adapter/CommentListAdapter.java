package com.gdgssu.assuk.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.NetworkImageView;
import com.gdgssu.assuk.AssukBaseData;
import com.gdgssu.assuk.R;
import com.gdgssu.assuk.manager.VolleySingleton;
import com.gdgssu.assuk.model.Question;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class CommentListAdapter extends BaseAdapter {
    private final static String TAG = "CommentListAdapter";

    private Question question;
    private Context context;

    private int like_id = 0;
    private Boolean like = false;

    public CommentListAdapter(JSONObject data_comment){
        this.question = new Question().convert(data_comment);
        setLikeState();
    }

    @Override
    public int getCount() {
        return question.comment.length() + 1;
    }

    @Override
    public Object getItem(int i) {
        return question;
    }

    @Override
    public long getItemId(int i) {
        return question.id;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        context = viewGroup.getContext();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(i == 0){
            view = inflater.inflate(R.layout.list_question, viewGroup, false);
            setQuestionLayout(view);
        }else{
            view = inflater.inflate(R.layout.list_comment, viewGroup, false);
            setCommentLayout(view, i);
        }
        return view;
    }

    private void setQuestionLayout(View view){
        NetworkImageView img_profile = (NetworkImageView)view.findViewById(R.id.img_profile);
        NetworkImageView img_content = (NetworkImageView)view.findViewById(R.id.img_content);
        TextView txt_userNick = (TextView)view.findViewById(R.id.txt_userNick);
        TextView txt_questionTitle = (TextView)view.findViewById(R.id.txt_questionTitle);
        TextView txt_questionContent = (TextView)view.findViewById(R.id.txt_questionContent);
        TextView txt_questionRegist = (TextView)view.findViewById(R.id.txt_questionRegist);

        LinearLayout btn_like = (LinearLayout)view.findViewById(R.id.linear_like);
        ImageView img_like = (ImageView)view.findViewById(R.id.img_like);
        TextView txt_like = (TextView)view.findViewById(R.id.txt_like);

        btn_like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggleLike();
            }
        });

        img_profile.setImageUrl(AssukBaseData.apiURL + question.user.profile,
                VolleySingleton.getInstance().getImageLoader());
        img_content.setImageUrl(AssukBaseData.apiURL + question.image,
                VolleySingleton.getInstance().getImageLoader());

        txt_userNick.setText(question.user.nick);
        txt_questionRegist.setText(question.regist.substring(0, 10)
                + " " + question.regist.substring(11, question.regist.length()));
        txt_questionTitle.setText(question.title);
        txt_questionContent.setText(question.content);

        if(question.comment.length() > 0){
            view.findViewById(R.id.no_comment).setVisibility(View.GONE);
        }

        if(like){
            btn_like.setBackgroundColor(Color.parseColor("#5b98eb"));
            img_like.setImageResource(R.drawable.icon_like_white);
            txt_like.setTextColor(Color.parseColor("#FFFFFF"));
        }else{
            btn_like.setBackgroundColor(Color.parseColor("#efefef"));
            img_like.setImageResource(R.drawable.icon_like);
            txt_like.setTextColor(Color.parseColor("#A5A5A5"));
        }
    }

    private void setCommentLayout(final View view, int i){
        try {
            final JSONObject data = question.comment.getJSONObject(i - 1);
            Log.d(TAG, data.toString());

            TextView txt_comment_content = (TextView)view.findViewById(R.id.txt_comment_content);
            final TextView txt_comment_userNick = (TextView)view.findViewById(R.id.txt_comment_userNick);
            final NetworkImageView img_comment_profile = (NetworkImageView)view.findViewById(R.id.img_comment_profile);

            txt_comment_content.setText(data.getString("commentContent"));

            AsyncHttpClient client = new AsyncHttpClient();
            client.get(context, AssukBaseData.apiURL + "/api/v1.0/user/" + data.getInt("user_id"), new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    if(statusCode == 200){
                        try {
                            String nick = response.getString("userNick");
                            String profile = response.getString("userProfile");

                            txt_comment_userNick.setText(nick);
                            img_comment_profile.setImageUrl(AssukBaseData.apiURL + profile,
                                    VolleySingleton.getInstance().getImageLoader());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setLikeState(){
        RequestParams param = AssukBaseData.flaskRestlessQuery("{\"filters\":[{\"name\":\"question_id\", \"op\":\"==\", \"val\":" + question.id + "}]}");

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(context, AssukBaseData.apiURL + "/api/v1.0/like", param, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                if(statusCode == 200){
                    try {
                        JSONArray data = response.getJSONArray("objects");
                        like = false;
                        for(int i = 0; i < data.length(); i++){
                            if(data.getJSONObject(i).getInt("user_id") == AssukBaseData.userID){
                                like_id = data.getJSONObject(i).getInt("id");
                                like = true;
                                break;
                            }
                        }
                        notifyDataSetChanged();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }


    private void toggleLike(){
        if(like){
            resetLike();
        }else{
            setLike();
        }
    }

    private void setLike(){
        JSONObject param = new JSONObject();
        try {
            param.put("question_id", question.id);
            param.put("user_id", AssukBaseData.userID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        StringEntity entity = null;
        try {
            entity = new StringEntity(param.toString(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/json");
        client.post(context, AssukBaseData.apiURL + "/api/v1.0/like", entity, "application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(TAG, statusCode + "");
                Log.d(TAG, response.toString());
                setLikeState();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                Log.d(TAG, response.toString());
                Toast.makeText(context, "'좋아요'에 실패했습니다. 잠시후 다시 시도하세요.", Toast.LENGTH_SHORT);
            }
        });
    }

    private void resetLike(){
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Content-Type", "application/json");
        client.delete(context, AssukBaseData.apiURL + "/api/v1.0/like/" + like_id, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d(TAG, statusCode + "");
                Log.d(TAG, response.toString());
                setLikeState();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject response){
                Log.d(TAG, response.toString());
                Toast.makeText(context, "잠시후 다시 시도하세요.", Toast.LENGTH_SHORT);
            }
        });
    }
}
